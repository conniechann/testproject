package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CountingStarsTest {
    @Test
    public void testCnt() throws Exception {
        int a = new CountingStars().digit(1,0);
        assertEquals("Error", Integer.MAX_VALUE, a);

    }

    @Test
    public void testCnt2() throws Exception {
    int a = new CountingStars().digit(9,3);
    assertEquals("Error", 3, a);

    }
}